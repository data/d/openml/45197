# OpenML dataset: win95pts_0

https://www.openml.org/d/45197

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Win95pts Bayesian Network. Sample 0.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-large.html#hepar2)

- Number of nodes: 76

- Number of arcs: 112

- Number of parameters: 574

- Average Markov blanket size: 5.92

- Average degree: 2.95

- Maximum in-degree: 7

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45197) of an [OpenML dataset](https://www.openml.org/d/45197). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45197/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45197/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45197/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

